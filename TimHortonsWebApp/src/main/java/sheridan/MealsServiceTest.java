package sheridan;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class MealsServiceTest {

	
	@Test
	public void testDrinksRegular() {
		List<String> temp =MealsService.getAvailableMealTypes(MealType.DRINKS);
	assertFalse(temp.isEmpty());
	}

	@Test
	public void testDrinksException() {
		assertEquals("No Brand Available", MealsService.getAvailableMealTypes(null).get(0));
	}
	
	
	  @Test public void testDrinksBoundaryIn()
	  { 
		  assertTrue(MealsService.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	  }
	  
	  @Test public void testDrinksBoundaryOut()
	  { 
		  assertTrue(MealsService.getAvailableMealTypes(null).size()  <2);
	  }
	 
}
