package sheridan;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(
        name = "selectmealtypeservlet",
        urlPatterns = "/SelectMealType"
)

public class SelectMealTypeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1831978487671842619L;
	
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String mealTypeTxt = req.getParameter("Type").replaceAll( " ", "" ).toUpperCase( );

        MealsService mealService = new MealsService( );
        MealType mealType = MealType.valueOf( mealTypeTxt );

        List<String> mealTypes = mealService.getAvailableMealTypes( mealType );

        req.setAttribute( "meals" , mealTypes);
        RequestDispatcher view = req.getRequestDispatcher( "result.jsp" );
        view.forward(req, resp);
    }	

}
